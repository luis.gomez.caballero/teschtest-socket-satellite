package techtest.socketsat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SocketSatelliteApplication {

	public static void main(String[] args) {
		SpringApplication.run(SocketSatelliteApplication.class, args);
	}

}
